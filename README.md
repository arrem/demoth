# DeMoth
DeMoth is a SAT-based model checker, supporting both bounded and unbounded model checking, made for the Computer-Aided
Verification course at TU Vienna, in the summer semester of 2020.

The unbounded model checker is largely based on the ideas presented in the following papers: 
* [McMillan, Kenneth L. "Interpolation and SAT-based model checking." International Conference on Computer Aided Verification. Springer, Berlin, Heidelberg, 2003.](http://mcmil.net/pubs/CAV03.pdf)
* [Gange, Graeme, et al. "Unbounded model-checking with interpolation for regular language constraints." International Conference on Tools and Algorithms for the Construction and Analysis of Systems. Springer, Berlin, Heidelberg, 2013.](https://jorgenavas.github.io/papers/regex-tacas13.pdf)

Note that the project currently only supports Unix systems and Windows 10 with the
[Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/install-win10). This is due to the modified
version of MiniSat that this project requires, which fails to build correctly on Windows.

## Building the project
DeMoth is written in Kotlin 1.4 using the Gradle build system. To build the project, either use one of the 
[Gradle startup scripts](https://docs.gradle.org/current/userguide/gradle_wrapper.html) bundled with this project
 (`gradlew` or `gradlew.bat`), or manually [get the latest version of Gradle](https://gradle.org/install/).

To build the project, navigate to the project directory and run:
```
./gradlew buildFatJar
```
(or a different appropriate command if you are building on Windows or building with a local installation of Gradle).
Alternatively, you can immediately build and run the project by invoking:
```
./gradlew run --args='<program arguments here>'
```

## Usage

DeMoth supports both bounded and unbounded model checking. The usage string demonstrates the arguments that
the checker supports:
```
USAGE: demoth <bmc|ubmc> <file> [k] [options]
 DeMoth is capable of running a bounded and an unbounded model checker. The first parameter controls
 which mode the program will be running in. If running in bounded model checker mode, the parameter k,
 which represents the bound up to which to check the circuit, must be specified.

Options:
 --all  Check all iterations up to the k-th for the unbounded model checker.
```

Gradle can be used to build and run the project. The easiest way to test the project out is to use the Gradle run task,
for example:
```
./gradlew run --args='bmc examples/latch.aag 2'
```


## Things to check out

This section outlines sections of the code that are worth checking out if you want to learn about how
the project works. The code is generously commented, includes design rationale and the comments were written targeting
the course staff that graded it. As such, the comments focus more on what is being done and why certain decisions were
made than standard code documentation would.

Starting from the `at.tuwien.arrem.demoth` directory:
* `parser/AigerFileParser.kt` - is the parser we use to parse ASCII AIGER files.
* `logic/BoundedModelChecker.kt` - is the backend for the bounded model checker which neatly integrates all the
required components, and thus serves as a good place to start reading about the BMC.
* `logic/AigerFormulaConstructor.kt` - translates parsed AIGER files into propositional formulas.
* `logic/UnboundedModelChecker.kt` - is the backend for the unbounded model checker. Similar to its bounded equivalent,
this class is a good starting point if you wish to look into the UBMC.
* `logic/SatSolver.kt` - invokes MiniSat to decide satisfiability of DIMACS files and generates proof trees.