package at.tuwien.arrem.demoth.logic

import at.tuwien.arrem.demoth.parser.Literal
import java.lang.IllegalStateException

class TseitinEncoder(totalVars: Int) {
    var aliasCounter = totalVars + 1

    private fun convert(formula: Formula, root: Boolean = true): Pair<FormulaLiteral, Formula> {
        if (formula is FormulaLiteral) {
            return formula to formula
        }

        val alias = FormulaLiteral(aliasCounter++, polarity = true)

        if (formula is BinaryFormula) {
            val (f, firstFormula) = convert(formula.left, false)
            val (s, secondFormula) = convert(formula.right, false)

            val cnf = when (formula.operator) {
                Operator.Or -> Formula.and(
                    Formula.or(-f, alias),
                    Formula.or(-s, alias),
                    Formula.or(-alias, f, s)
                )

                Operator.And -> Formula.and(
                    Formula.or(-alias, f),
                    Formula.or(-alias, s),
                    Formula.or(-f, -s, alias)
                )

                Operator.Equivalence -> Formula.and(
                    Formula.or(-alias, -f, s),
                    Formula.or(-alias, -s, f),
                    Formula.or(-f, -s, alias),
                    Formula.or(f, s, alias)
                )

                Operator.Implication -> Formula.and(
                    Formula.or(f, alias),
                    Formula.or(-s, alias),
                    Formula.or(-alias, -f, s)
                )
            }

            val list = mutableListOf<Formula>().apply {
                (firstFormula as? BinaryFormula)?.let { add(it) }
                (secondFormula as? BinaryFormula)?.let { add(it) }
                add(cnf)
                if (root) {
                    add(alias)
                }
            }

            return alias to Formula.and(*list.toTypedArray())
        }

        if (formula is Negated) {
            val (q, converted) = convert(formula.formula, false)
            val cnf = Formula.and(
                Formula.or(-alias, -q),
                Formula.or(q, alias)
            )

            val list = mutableListOf<Formula>().apply {
                (converted as? BinaryFormula)?.let { add(it) }
                add(cnf)
            }

            return alias to Formula.and(*list.toTypedArray())
        }

        throw IllegalStateException("Unsupported formula type.")
    }

    fun convert(formula: Formula): Formula {
        return convert(formula, true).second
    }
}
