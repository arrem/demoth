package at.tuwien.arrem.demoth.logic

import at.tuwien.arrem.demoth.parser.AigerFileParser
import org.apache.logging.log4j.LogManager
import java.nio.file.Path
import kotlin.math.absoluteValue
import kotlin.math.sign

/**
 * Provides capabilities for running an unbounded model checker on AIGER files. Takes in an `inputPath` representing the
 * path of the `.aag` file containing the description of the circuit to check.
 *
 * This class is used as a backend for the unbounded model checker. Its [runModelCheck] method fully ties all of the
 * necessary components to check the provided circuit.
 */
class UnboundedModelChecker(inputPath: Path) {
    private val log = LogManager.getLogger(BoundedModelChecker::class.simpleName)
    private val file = AigerFileParser().parse(inputPath)

    init {
        log.trace("Parsed AIGER binary file:\n$file")
    }

    /**
     * Runs the model checker algorithm on the circuit provided in the constructor. This method iterates all `k` until
     * it founds a bound that will prove or disprove the safety property. As such, the method will not terminate
     * automatically and may take a really long time to finish for complex circuits.
     */
    fun runModelCheck(): Boolean {
        (1..Int.MAX_VALUE).forEach { k ->
            val result = unboundedCheck(k)

            if (result != null) {
                return result
            }
        }

        throw IllegalStateException("Model checker timed out without finding a proof.")
    }

    /**
     * This method runs the unbounded model checker on a given bound, returning either a boolean that indicates whether
     * the property holds, or if the test was inconclusive, `null` is returned and the [runModelCheck] method will
     * repeat the test with a higher value of `k`.
     */
    private fun unboundedCheck(k: Int): Boolean? {
        val constructor = AigerFormulaConstructor(file)
        val clauses = constructor.constructFormula(k)

        val dimacsConstructor = DIMACSFormulaConstructor(file.maxVars, k)
        val solver = SatSolver()

        val (initial, rest) = clauses.partition { it.part == Part.Initial }

        var r = initial
        var first = true

        while (true) {
            val newClauses = r + rest
            val dimacs = dimacsConstructor.convert(newClauses)
            val output = solver.solve(dimacs)

            if (output.satisfiable) {
                return if (first) { false } else { null }
            }

            val interpolant = computeInterpolant(output.proof, newClauses)
            val substitute = substituteInterpolant(interpolant, constructor)

            log.debug("Please check whether $substitute implies $r")


            if (implies(substitute, r, k)) {
                return true
            }

            r = mergeDisjunctive(r, substitute).map { Clause(it.literals, Set.A, Part.Initial) }
            first = false
        }
    }

    private fun implies(antecedent: List<Clause>, consequent: List<Clause>, k: Int): Boolean {
        if (antecedent.any { it.literals.isEmpty() }) {
            return true
        }

        val convertedAntecedent = clauseListToFormula(antecedent)
        val convertedConsequent = clauseListToFormula(consequent)
        val formula = BinaryFormula(convertedAntecedent, Operator.And, Negated(convertedConsequent))

        val tseitinEncoder = TseitinEncoder((file.maxVars + 1) * (k + 1))
        val encodedFormula = tseitinEncoder.convert(formula)

        val clauses = formulaToClauseList(encodedFormula)
        val constructor = DIMACSFormulaConstructor(file.maxVars, k, tseitinEncoder.aliasCounter)
        val dimacs = constructor.convert(clauses)

        val solver = SatSolver()
        return !solver.solve(dimacs).satisfiable
    }

    private fun clauseListToFormula(formula: List<Clause>): Formula {
        return Formula.and(*formula.map {
            Formula.or(*it.literals.map { l -> FormulaLiteral(l.absoluteValue, l.sign == 1) }.toTypedArray())
        }.toTypedArray())
    }

    private fun formulaToClauseList(formula: Formula): List<Clause> {
        return listOf(formula).conjunctions().flatten().map { clause ->
            Clause(clause.map { it.label * if (it.polarity) { 1 } else { -1 } })
        }
    }

    private fun List<Formula>.conjunctions(): List<Formula> {
        return flatMap {
            when (it) {
                is FormulaLiteral -> listOf(it)
                is BinaryFormula -> {
                    when (it.operator) {
                        Operator.Or -> listOf(it)
                        Operator.And -> listOf(it.left).conjunctions() + listOf(it.right).conjunctions()
                        else -> error("Unreachable.")
                    }
                }
                else -> error("Unreachable.")
            }
        }
    }

    private fun List<Formula>.flatten(): List<List<FormulaLiteral>> {
        return map {
            when (it) {
                is FormulaLiteral -> listOf(it)
                is BinaryFormula -> {
                    require(it.operator == Operator.Or)
                    (listOf(it.left).flatten() + listOf(it.right).flatten()).flatten()
                }
                else -> error("Unreachable.")
            }
        }
    }


    /**
     * Runs the state substitution on the given [interpolant], replacing all relevant variables from state 1 into
     * equivalent variables in state 0.
     */
    private fun substituteInterpolant(interpolant: List<Clause>, constructor: AigerFormulaConstructor): List<Clause> {
        return unitPropagation(interpolant.map { clause ->
            Clause(constructor.clauseToAiger(clause).map { al ->
                val state = if (al.state == 1 && al.literal.label != 0) { 0 } else { al.state }

                val sign = if (al.literal.negated) { -1 } else { 1 }
                val identifier = 1 + al.literal.label + (file.maxVars + 1) * state

                sign * identifier
            })
        }.distinct())
    }


    /**
     * Computes an interpolant for the given set of [clauses], already annotated with [Set] fields, based on the
     * [proofTree] that is generated by a modified MiniSat solver.
     */
    private fun computeInterpolant(proofTree: List<String>, clauses: List<Clause>): List<Clause> {
        // Compute the set of variables which are in B-clauses.
        val bSet = clauses.asSequence()
            .filter { it.set == Set.B }
            .flatMap { it.literals.map { l -> l.absoluteValue } }
            .toSet()

        // Used to check which clauses are in which set, since the generated proof will not necessarily use all input
        // clauses. As such, we compare clauses by their literals and resolve sets in that way.
        val clauseSets = clauses.associate {
            it.literals.joinToString(" ") to it.set
        }

        val labels = mutableMapOf<String, List<Clause>>()

        for (line in proofTree) {
            val split = line.split(" ", limit = 3)

            // According to whether the line is a ROOT or a CHAIN line, the appropriate handler method is invoked.
            when(split[1]) {
                "ROOT" -> handleRootLabel(clauseSets, split, bSet, labels)
                "CHAIN" -> handleChainLabel(split, labels, bSet)
                else -> throw IllegalStateException("Invalid proof tree label, expected ROOT or CHAIN.")
            }
        }

        // When the entire proof tree has been traversed, the label of the last node will be the generated interpolant.
        val last = proofTree.last().split(" ", limit = 2)[0]
        val lastLabel = labels[last] ?: throw IllegalStateException("Last node of proof tree was unlabeled.")

        log.debug("Generated interpolant $lastLabel.")
        return lastLabel
    }

    /**
     * Computes and assigns the appropriate label for ROOT proof nodes.
     */
    private fun handleRootLabel(
        clauseSets: Map<String, Set?>,
        split: List<String>,
        bSet: kotlin.collections.Set<Int>,
        labels: MutableMap<String, List<Clause>>
    ) {
        val set = clauseSets[split[2]] ?: throw IllegalStateException("Clause sorting failed on clause ${split[2]}.")

        log.debug("Clause ${split[0]} was ROOT clause in set $set.")

        if (set == Set.A) {
            // For leaves in A, the label is obtained by keeping only the variables in B.
            val remaining = split[2].split(" ").asSequence()
                .map { it.toInt() }
                .filter { it.absoluteValue in bSet }
                .toList()
                .takeUnless(List<Int>::isEmpty)
                ?: listOf(-1)

            log.debug("Labeled by $remaining.")
            labels[split[0]] = listOf(Clause(remaining))
        } else {
            // For leaves in B, the label is the constant true, which we represent by the literal 1.
            log.debug("Labeled by [1].")
            labels[split[0]] = listOf(Clause(1))
        }
    }

    /**
     * Computes and assigns the appropriate label for CHAIN proof nodes.
     */
    private fun handleChainLabel(split: List<String>,
                                 labels: MutableMap<String, List<Clause>>,
                                 bSet: kotlin.collections.Set<Int>
    ) {
        log.debug("Clause ${split[0]} was CHAIN clause.")

        val chain = split[2].split(" =>")[0].split(" ")

        // Sanity check: A chain must have at least 3 components, since the minimal-sized chain has two clauses
        // and a resolvent. Any bigger chain has an additional 2 components for every step.
        require(chain.size >= 3 && chain.size % 2 == 1)

        val iterator = chain.iterator()

        // We start by popping one element from the iterator, then in every iteration, we grab the resolvent and the
        // other clause that was used to do the resolution step. This allows us to resolve the entire chain and compute
        // the label for it.
        val head = iterator.next()
        var labelHead = labels[head] ?: throw IllegalStateException("Encountered unlabeled clause $head.")

        while (iterator.hasNext()) {
            // Parse set of resolvent variable.
            val conjunctive = iterator.next().trim('[', ']').toInt() in bSet

            val next = iterator.next()
            val labelNext = labels[next] ?: throw IllegalStateException("Encountered unlabeled clause $next.")
            val label = merge(labelHead, labelNext, conjunctive)

            log.debug("Chain step labeled by $label.")
            labelHead = label
        }

        log.debug("Chain labeled by $labelHead")
        labels[split[0]] = labelHead
    }

    /**
     * A utility function for merging two sets of clauses either conjunctively or disjunctively.
     */
    private fun merge(first: List<Clause>, second: List<Clause>, conjunctive: Boolean): List<Clause> {
        return if (conjunctive) {
            mergeConjunctive(first, second)
        } else {
            mergeDisjunctive(first, second)
        }
    }

    private fun mergeDisjunctive(first: List<Clause>, second: List<Clause>): List<Clause> {
        log.debug("Disjunctively merging $first and $second.")

        val clauses = second.flatMap { n ->
            first.map { h ->
                // Merge two literal sets disjunctively. Remove all -1 (false) literals from a disjunction since
                // they're superfluous, and deduplicate literal list in case sources had repeated literals.
                val literals = (h.literals + n.literals).asSequence()
                    .filter { it != -1 }
                    .distinct()
                    .sortedWith(compareBy({ it.absoluteValue }, { it }))
                    .toList()

                when {
                    literals.isEmpty() -> Clause(-1)
                    literals.any { it == 1 } -> Clause(1)
                    else -> Clause(literals.takeIf(List<Int>::isNotEmpty) ?: listOf(1, -1))
                }
            }
        }.distinct()

        // Unit propagation can still be used to eliminate some redundancies and end up with a smaller formula.
        // Consider for example the case where the sets [{8}, {4}] and [{8}, {4}] should be merged disjunctively.
        // With the duplicate elimination, this results in the sets {8}, {4 8}, {4 8}, {4}. Unit propagation can
        // eliminate the second and third clauses, which are both redundant.
        return unitPropagation(clauses)
    }

    private fun mergeConjunctive(first: List<Clause>, second: List<Clause>): List<Clause> {
        log.debug("Conjunctively merging $first and $second.")

        // For the conjunctive merge case, we can simply join the two sets of clauses, taking care to optimize
        // for duplicate values, and True/False constants.
        val merged = first.asSequence().plus(second)
            .distinct()
            .filter { it != Clause(1) }
            .toList()

        // A conjunction that contains false simplifies to false:
        if (Clause(-1) in merged) {
            return listOf(Clause(-1))
        }

        // Otherwise return clause or 1 (true) if the clause is empty.
        return unitPropagation(merged).takeUnless(List<Clause>::isEmpty) ?: listOf(Clause(1))
    }

    /**
     * A utility function that simplifies a list of [clauses] by applying unit propagation until it finds that there are
     * no more changes to the clause list. This method can be used to generate shorter formulas and thus make the
     * debug output a bit more comprehensible, as well as taking some of the workload from the SAT solver. In theory,
     * the unit propagation step is not needed for the functionality of the solver.
     */
    private fun unitPropagation(clauses: List<Clause>): List<Clause> {
        val unitLiterals = clauses.asSequence()
            .filter { it.literals.size == 1 }
            .map { it.literals.single() }
            .toList()

        if (unitLiterals.isEmpty()) {
            return clauses
        }

        // Check for changes by counting literals at start and comparing with optimized value.
        val initialCount = clauses.literalCount()

        val optimizedClauses = clauses.mapNotNull { clause ->
            // Do not touch unit clauses.
            if (clause.literals.size == 1) {
                return@mapNotNull clause
            }

            // Any clause that contains a unit literal with the same polarity can be removed:
            if (unitLiterals.any { it in clause.literals }) {
                return@mapNotNull null
            }

            // Any literal that is of opposite polarity to any of the unit literals can be removed:
            Clause(clause.literals.filter { (-it) in unitLiterals })
        }


        // If we have done any optimization, run another step of unit propagation, otherwise just return the result
        // as it is:
        return if (initialCount != optimizedClauses.literalCount()) {
            log.debug("Unit propagation optimized set $clauses to $optimizedClauses.")
            unitPropagation(optimizedClauses)
        } else {
            optimizedClauses
        }
    }

    private fun List<Clause>.literalCount() = sumBy { it.literals.size }
}
