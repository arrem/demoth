package at.tuwien.arrem.demoth.logic

import at.tuwien.arrem.demoth.parser.AigerFile
import at.tuwien.arrem.demoth.parser.Literal
import kotlin.math.absoluteValue
import kotlin.math.sign

/**
 * Represents the A and B sets, as used in the unbounded model checker for the interpolant generation step.
 */
enum class Set {
    A,
    B
}

/**
 * Represents the part of a formula that a certain clause belongs to, either initial, transition or final. This is used
 * to categorize clauses, so that the clauses categorized as initial can be combined with the interpolant in the
 * unbounded model checker.
 */
enum class Part {
    Initial,
    Transition,
    Final
}

/**
 * A clause is a disjunction of [literals]. The literals here are already encoded in DIMACS format, as positive or
 * negative integers. Additionally, every clause optionally carries information about which [Set] it belongs to.
 */
class Clause(
    literals: List<Int>,
    val set: Set? = null,
    val part: Part? = null,
) {
    // Since MiniSat sorts literals in clauses, we do the same so that we can do more efficient clause matching while
    // resolving MiniSat's proof tree and generating the interpolant.
    val literals = literals.sortedWith(compareBy ({ it.absoluteValue }, { it }))

    constructor(vararg literal: Int, set: Set? = null, part: Part? = null) : this(literal.toList(), set, part)

    override fun toString() = literals.joinToString(" ", prefix = "{", postfix = "}")

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Clause) return false

        if (literals == other.literals) return true

        if (literals.size != other.literals.size) {
            return false
        }

        val sort = literals.asSequence().sortedWith(compareBy ({ it.absoluteValue }, { it }))
        val sortOther = other.literals.asSequence().sortedWith(compareBy ({ it.absoluteValue }, { it }))

        return sort.zip(sortOther).all { (p1, p2) -> p1 == p2 }
    }

    override fun hashCode(): Int {
        return literals.hashCode()
    }
}

/**
 * Creates a formula representing the circuit in a given AIGER file. The [constructFormula] method is used with a
 * provided bound to generate a formula for up to k iterations.
 */
class AigerFormulaConstructor(val file: AigerFile) {
    /**
     * Converts the given AIGER file into a set of conjunctive clauses, encoding the circuit as a CNF formula.
     *
     * We chose not to use a two step approach where a propositional formula is generated and then converted into CNF
     * through a Tseitin transformation for two reasons. Firstly, Tseitin transformation is a lot easier to implement
     * recursively, and tests on the JVM have shown that this requires quite a large stack to be able to handle cases
     * up to k = 30.
     *
     * Moreover, directly translating formulas into CNF by hand saves computation time and results in the final solver
     * being a lot faster, by quite a big margin. Since the formulas we are dealing with contain no nested equivalence
     * operations, we do not have an exponential blow-up that would benefit from a Tseitin transformation. All of our
     * formulas are simple enough to be easily translatable by hand, which is the approach that we have opted for.
     *
     * To convert a circuit into a SAT instance, this function does the following:
     * 1. It is asserted that in the initial step, all latch outputs are zero, as per the specification in the project
     * description.
     * 2. On each step, the value of an AND gate's output is the value of its inputs AND-ed together. The CNF
     * transformation for this and the next step are outlined in the comments.
     * 3. In step `k + 1`, the value of a latch's `current_value` variable is the value of its `next_value` variable
     * in step `k`.
     * 4. Since we opted to model the true/false constant as a variable and its negation respectively, we need to
     * ensure that this variable has the value `true` throughout the simulation.
     * 5. The safety property is encoded, that is, the bad state detector bit is 1 in any of the steps.
     */
    fun constructFormula(bound: Int): List<Clause> {
        // 1. Initial state latches:
        val initialLatches = file.latches.map {
            Clause(it.currentValue.withPolarity(false).toDIMACS(0), set = Set.A, part = Part.Initial)
        }

        // 2. And gate input output hooks:
        // (CNF) of A ↔ (B ∧ C)
        // (¬B ∨ ¬C ∨ A) ∧ (¬A ∨ B) ∧ (¬A ∨ C)
        val ands = (0..bound).flatMap { s ->
            val set = if (s == 0 || s == 1) { Set.A } else { Set.B }
            val part = if (s == 0) { Part.Initial } else { Part.Transition }

            file.andGates.flatMap { g ->
                listOf(
                        Clause((-g.firstIn).toDIMACS(s), (-g.secondIn).toDIMACS(s), g.output.toDIMACS(s), set = set, part = part),
                        Clause((-g.output).toDIMACS(s), g.firstIn.toDIMACS(s), set = set, part = part),
                        Clause((-g.output).toDIMACS(s), g.secondIn.toDIMACS(s), set = set, part = part),
                )
            }
        }

        // 3. Latch current and next state hooks:
        // (CNF) of A ↔ B
        // (¬B ∨ A) ∧ (¬A ∨ B)
        val latches = (0 until bound).flatMap { s ->
            val set = if (s == 0) { Set.A } else { Set.B }
            file.latches.flatMap { l ->
                listOf(
                        Clause((-l.nextValue).toDIMACS(s), l.currentValue.toDIMACS(s + 1), set = set, part = Part.Transition),
                        Clause((-l.currentValue).toDIMACS(s + 1), l.nextValue.toDIMACS(s), set = set, part = Part.Transition),
                )
            }
        }

        // 4. True wire stays true:
        val trues = (0..bound).map { s ->
            Clause(Literal.fromAigerInteger(1).toDIMACS(s), set = Set.A, part = Part.Transition)
        }

        // 5. Safety property:
        val safety = Clause((0..bound).map { s -> file.outputs[0].toDIMACS(s) }, set = Set.B, part = Part.Final)

        return initialLatches + ands + latches + trues + safety
    }

    /**
     * Converts a given AIGER literal into its DIMACS representation in the given state [k].
     */
    private fun Literal.toDIMACS(k: Int): Int {
        val sign = if (negated) { -1 } else { 1 }
        val identifier = 1 + label + (file.maxVars + 1) * k

        return sign * identifier
    }

    /**
     * Represents an AIGER [Literal] together with the state that it is in. This representation is convenient when
     * dealing with switches from DIMACS to AIGER which is needed to perform the state substitution in the interpolant.
     */
    data class AigerLiteral(val literal: Literal, val state: Int)

    /**
     * Converts a clause back into a list of AIGER literals (containing their AIGER label and polarity), and the
     * state that each literal was in. This is used to decompose the generated interpolant so tha the switch from
     * state 1 into state 0 can be made.
     */
    fun clauseToAiger(clause: Clause): List<AigerLiteral> {
        return clause.literals.map { l ->
            val sign = l.sign
            val value = l.absoluteValue

            val label = (value - 1) % (file.maxVars + 1)
            val state = (value - 1) / (file.maxVars + 1)

            AigerLiteral(Literal(label, sign == -1), state)
        }
    }
}
