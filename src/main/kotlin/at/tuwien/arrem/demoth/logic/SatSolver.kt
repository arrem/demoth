package at.tuwien.arrem.demoth.logic

import kotlin.system.exitProcess

/**
 * Provides SAT solving capabilities by spawning a MiniSat process. It is strictly required that the modified version
 * of MiniSat is in the application's path, since it is needed for the unbounded model checker to work properly.
 *
 * Linux and MacOS should work without any problems. For Windows, due to issues compiling MiniSat, we only support
 * Windows 10 with the Windows Subsystem for Linux, since it allows us to execute the Linux version of the MiniSat
 * executable. Note that even with WSL2, this still makes the solver slower than when it is run on a fully supported
 * Unix system. Windows support is only for convenience.
 */
class SatSolver {
    /**
     * Runs the modified MiniSat solver on the given [dimacs] string and produces a [SatResponse] containing the
     * satisfiability status of the formula and an unsatisfiability proof if applicable.
     */
    fun solve(dimacs: String): SatResponse {
        val windows = System.getProperty("os.name").contains("Windows")

        if (windows && System.getProperty("os.name") != "Windows 10") {
            System.err.println("DeMoth currently only supports Windows 10 with the Linux subsystem.")
            exitProcess(1)
        }

        val builder = if (windows) {
            ProcessBuilder("wsl", "./minisat", "-c")
        } else {
            ProcessBuilder("./minisat", "-c")
        }

        val process = runCatching {
            builder.start()
        }.getOrElse {
            System.err.println("Error: Could not run minisat. Check if file is in path.")
            if (windows) {
                System.err.println("\tNote: DeMoth requires the Windows Subsystem for Linux to run on Windows 10.")
                System.err.println("\tSee: https://docs.microsoft.com/en-us/windows/wsl/install-win10")
            }
            exitProcess(1)
        }

        process.outputStream.bufferedWriter().use {
            it.write(dimacs)
        }

        val output = process.inputStream.bufferedReader().readText()

        return SatResponse.fromOutput(output)
    }
}

/**
 * A `SatResponse` contains the satisfiability status of the formula. If the formula is unsatisfiable, the modified
 * MinSat executable will also generate a proof, which will be cleaned up and stored as well. If the given formula
 * is satisfiable, the response will contain an empty list as a proof.
 */
data class SatResponse(val satisfiable: Boolean, val proof: List<String>) {
    companion object {
        fun fromOutput(output: String): SatResponse {
            val trimmed = output.trim().lines()
            return SatResponse("UNSATISFIABLE" !in output, trimmed.filter { it.isNotBlank() && it[0].isDigit() })
        }
    }
}
