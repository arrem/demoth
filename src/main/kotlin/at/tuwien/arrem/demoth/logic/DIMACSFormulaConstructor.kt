package at.tuwien.arrem.demoth.logic

import org.apache.logging.log4j.LogManager


/**
 * A short utility class for converting a list of DIMACS clauses to a DIMACS file. Most of the work this class does is
 * generating the correct DIMACS header, which depends on the number of variables in the AIGER circuit, as specified in
 * the input file, and the number of iterations for a particular problem, that is, the maximum value that k can take.
 */
class DIMACSFormulaConstructor(
    private val aigerVariables: Int,
    private val iterations: Int,
    private val additionalVariables: Int = 0,
) {
    private val log = LogManager.getLogger(DIMACSFormulaConstructor::class.simpleName)

    fun convert(cnf: List<Clause>): String {
        // 1 is added to the number of variables since we model the true wire as a variable. 1 is also added to max K
        // since variables range from 0 to K, inclusive.
        val nbVar = (1 + aigerVariables) * (iterations + 1) + additionalVariables
        val nbClauses = cnf.size

        return buildString {
            appendLine("p cnf $nbVar $nbClauses")

            cnf.forEach {
                val generated = it.literals.joinToString(" ", postfix = " 0")

                log.debug("$it maps to $generated")
                appendLine(generated)
            }
        }
    }
}
