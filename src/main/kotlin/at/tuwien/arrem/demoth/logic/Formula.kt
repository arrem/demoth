package at.tuwien.arrem.demoth.logic

interface Formula {
    companion object {
        fun join(operator: Operator, formulas: Iterable<Formula>): Formula {
            if (formulas.count() == 1) {
                return formulas.single()
            }

            val first = formulas.take(2).let { (f, s) ->
                BinaryFormula(
                    f,
                    operator,
                    s
                )
            }

            return formulas.drop(2).fold(first) { acc, l ->
                BinaryFormula(acc, operator, l)
            }
        }

        fun and(vararg literals: Formula) = join(Operator.And, literals.toList())
        fun or(vararg literals: Formula) = join(Operator.Or, literals.toList())
    }
}

enum class Operator(private val value: String) {
    And("∧"),
    Or("∨"),
    Equivalence("↔"),
    Implication("→");

    override fun toString() = value
}

data class FormulaLiteral(val label: Int, val polarity: Boolean) : Formula {
    operator fun unaryMinus() = copy(polarity = !polarity)

    override fun toString() = "${if (polarity) "-" else ""}$label"
}

data class Negated(val formula: Formula): Formula {
    override fun toString() = "-$formula"
}

data class BinaryFormula(val left: Formula, var operator: Operator, val right: Formula) : Formula {
    override fun toString() = "[$left $operator $right]"
}
