package at.tuwien.arrem.demoth.logic

import at.tuwien.arrem.demoth.parser.AigerFileParser
import org.apache.logging.log4j.LogManager
import java.nio.file.Path

/**
 * Provides capabilities for running a bounded model checker on AIGER files. Takes in an `inputPath` representing the
 * path of the `.aag` file containing the description of the circuit to check.
 *
 * This class is used as a backend for the bounded model checker. Its [runModelCheck] method fully ties all of the
 * necessary components to check the provided circuit.
 */
class BoundedModelChecker(inputPath: Path) {
    private val log = LogManager.getLogger(BoundedModelChecker::class.simpleName)
    private val file = AigerFileParser().parse(inputPath)

    init {
        log.trace("Parsed AIGER binary file:\n$file")
    }

    /**
     * Runs the bounded model checker for a given [bound], by constructing the bound-appropriate formula from the AIGER
     * file specified in the constructor, converting the generated clauses into DIMACS and running them through a
     * modified MiniSat solver.
     *
     * Returns a boolean indicating whether the circuit specified in the file is safe or not.
     *
     * Note: For the sake of simplicity, we chose not to bundle two versions of MiniSat and always use the modified proof
     * logging version. In theory, a performance gain could be accomplished by using the regular MiniSat which should
     * be faster due to its lack of proof checking.
     */
    fun runModelCheck(bound: Int): Boolean {
        val constructor = AigerFormulaConstructor(file)
        val clauses = constructor.constructFormula(bound)

        log.trace("Constructed formula: $clauses")

        val dimacsConstructor = DIMACSFormulaConstructor(file.maxVars, bound)
        val dimacs = dimacsConstructor.convert(clauses)

        log.trace("Dimacs output:\n$dimacs")

        val solver = SatSolver()
        val output = solver.solve(dimacs)

        return !output.satisfiable
    }
}
