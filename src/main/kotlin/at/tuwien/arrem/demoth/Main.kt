package at.tuwien.arrem.demoth

import at.tuwien.arrem.demoth.logic.*
import java.nio.file.Paths
import kotlin.system.exitProcess

/**
 * Displays the usage message, outlining how to correctly use the program.
 */
fun usage() {
    System.err.println("""
        | USAGE: demoth <bmc|ubmc> <file> [k] [options]
        | DeMoth is capable of running a bounded and an unbounded model checker. The first parameter controls
        | which mode the program will be running in. If running in bounded model checker mode, the parameter k,
        | which represents the bound up to which to check the circuit, must be specified.
        | 
        | Options:
        | ${"\t"}--all${"\t"}Check all iterations up to the k-th for the unbounded model checker.
    """.trimMargin())
}

/**
 * The main function serves as the entry point for the program, and invokes either the bounded or the unbounded model
 * checking procedure, depending on which arguments were provided by the user.
 */
fun main(args: Array<String>) {
    if (args.size < 2) {
        usage()
        exitProcess(1)
    }

    when(args[0]) {
        "bmc" -> {
            bmc(args)
        }

        "ubmc" -> {
            ubmc(args)
        }

        else -> {
            usage()
            exitProcess(1)
        }
    }
}

/**
 * The handler for the unbounded model checker. Simply reads out the path and invokes the [UnboundedModelChecker] which
 * does all of the useful work.
 */
fun ubmc(args: Array<String>) {
    val path = args[1]
    val checker = UnboundedModelChecker(Paths.get(path))

    val okay = checker.runModelCheck()

    println(if (okay) "OK" else "FAIL")
}

/**
 * The handler for the bounded model checker. Reads out the path and the bound and invokes the [BoundedModelChecker]
 * which does all of the useful work.
 *
 * Normally, the frontend should be used without the --all flag, which only exists for debugging purposes. If the flag
 * is set, the frontend will iterate all the k from 0 until the specified bound and run a check for each k, not stopping
 * even if one of the steps reports a failure. This mode is intended to be used to verify correctness against other
 * reference implementations.
 */
fun bmc(args: Array<String>) {
    val path = args[1]
    val bound = args[2].toIntOrNull()

    if (bound == null) {
        usage()
        exitProcess(1)
    }

    val checker = BoundedModelChecker(Paths.get(path))

    if (!args.contains("--all")) {
        for (k in 0..bound) {
            val okay = checker.runModelCheck(k)

            if (!okay) {
                println("FAIL")
                exitProcess(0)
            }
        }

        println("OK")
        return
    }

    for (k in 0..bound) {
        val okay = checker.runModelCheck(k)
        println("For k=$k")
        println(if (okay) "OK" else "FAIL")
    }
}

