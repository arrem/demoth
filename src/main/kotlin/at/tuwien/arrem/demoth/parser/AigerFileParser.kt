package at.tuwien.arrem.demoth.parser

import at.tuwien.arrem.demoth.logic.Formula
import java.lang.Exception
import java.nio.file.Path

/**
 * Provides parsing capabilities for the ASCII Aiger file format.
 */
class AigerFileParser {
    fun parse(path: Path): AigerFile {
        val lines = path.toFile().readLines()

        val header = lines.firstOrNull() ?: throw AigerParserError("An empty file was provided.")

        if (!header.startsWith("aag ")) {
            throw AigerParserError("The provided file does not have the ASCII Aiger header aag.")
        }

        val headerSplit = header.split(" ")

        if (headerSplit.size != 6) {
            throw AigerParserError(
                "The header of the provided file is malformed. Expected 5 parameters, got ${headerSplit.size}."
            )
        }

        val (maxVars, inputs, latches, outputs, andGates) = headerSplit.asSequence().drop(1).map {
            it.toIntOrNull() ?: throw AigerParserError("The file header contains a non-integer parameter: '$it'.")
        }.toList()

        val parsedInputs = lines.asSequence().drop(1).take(inputs).map {
            val int = it.toIntOrNull() ?: throw AigerParserError("Input specification contains a non-integer: '$it'.")
            Literal.fromAigerInteger(int)
        }.toList()

        if (parsedInputs.size != inputs) {
            throw AigerParserError("Invalid number of input lines, expected $inputs got ${parsedInputs.size}.")
        }

        val parsedLatches = lines.asSequence().drop(1 + inputs).take(latches).map {
            val split = it.split(" ")

            if (split.size != 2) {
                throw AigerParserError("Latch specification is malformed. Expected 2 parameters got ${split.size}.")
            }

            val q = split[0].toIntOrNull()
                ?: throw AigerParserError("Latch specification contains a non-integer: '${split[0]}'.")

            if (q % 2 != 0) {
                throw AigerParserError("Latch specification contains odd value for Q: '$q'.")
            }

            val notQ = split[1].toIntOrNull()
                ?: throw AigerParserError("Latch specification contains a non-integer: '${split[1]}'.")

            Latch(Literal.fromAigerInteger(q), Literal.fromAigerInteger(notQ))
        }.toList()

        if (parsedLatches.size != latches) {
            throw AigerParserError("Invalid number of latch lines, expected $latches got ${parsedLatches.size}.")
        }

        val parsedOutputs = lines.asSequence().drop(1 + inputs + latches).take(outputs).map {
            val int = it.toIntOrNull() ?: throw AigerParserError("Output specification contains a non-integer: '$it'.")
            Literal.fromAigerInteger(int)
        }.toList()

        if (parsedOutputs.size != outputs) {
            throw AigerParserError("Invalid number of output lines, expected $outputs got ${parsedOutputs.size}.")
        }

        val parsedAndGates = lines.asSequence().drop(1 + inputs + latches + outputs).take(andGates).map {
            val split = it.split(" ")

            if (split.size != 3) {
                throw AigerParserError("And gate specification is malformed. Expected 3 parameters got ${split.size}.")
            }

            val out = split[0].toIntOrNull()
                ?: throw AigerParserError("And gate specification contains a non-integer: '${split[0]}'.")

            if (out % 2 != 0) {
                throw AigerParserError("And gate specification contains odd value for output: '$out'.")
            }

            val firstIn = split[1].toIntOrNull()
                ?: throw AigerParserError("And gate specification contains a non-integer: '${split[1]}'.")

            val secondIn = split[2].toIntOrNull()
                ?: throw AigerParserError("And gate specification contains a non-integer: '${split[2]}'.")

            AndGate(
                Literal.fromAigerInteger(out),
                Literal.fromAigerInteger(firstIn),
                Literal.fromAigerInteger(secondIn)
            )
        }.toList()

        if (parsedAndGates.size != andGates) {
            throw AigerParserError("Invalid number of and gate lines, expected $andGates got ${parsedAndGates.size}.")
        }

        return AigerFile(maxVars, parsedInputs, parsedLatches, parsedOutputs, parsedAndGates)
    }
}

data class AigerFile(
    val maxVars: Int,
    val inputs: List<Literal>,
    val latches: List<Latch>,
    val outputs: List<Literal>,
    val andGates: List<AndGate>
) {
    override fun toString(): String {
        return buildString {
            append("Inputs [${inputs.size}]: ")
            appendLine(inputs.joinToString())

            append("Latches [${latches.size}]: ")
            appendLine(latches.joinToString())

            append("Outputs [${outputs.size}]: ")
            appendLine(outputs.joinToString())

            append("And gates [${andGates.size}]: ")
            appendLine(andGates.joinToString())
        }
    }
}

/**
 * Represents a propositional literal. The [label] is the variable index as specified by Aiger, and [negated] specifies
 * if the literal is logically negated or not.
 */
data class Literal(val label: Int, val negated: Boolean) : Formula {
    companion object {
        fun fromAigerInteger(int: Int): Literal {
            return if (int == 0 || int == 1) {
                Literal(0, int % 2 == 0)
            } else {
                Literal(int / 2, int % 2 == 1)
            }
        }
    }

    fun withPolarity(positive: Boolean) = copy(negated = !positive)

    operator fun unaryMinus() = copy(negated = !negated)

    override fun toString() = "${if (negated) "-" else ""}$label"
}

/**
 * Represents a latch storing 1 bit of memory. [currentValue] is an always positive literal specifying which variable
 * contains the current state of the literal, whereas [nextValue] specifies which literal (positive or negative)
 * determines the latch's next state.
 */
data class Latch(val currentValue: Literal, val nextValue: Literal) {
    override fun toString() = "[current: $currentValue, next: $nextValue]"
}

/**
 * Represents an and gate with two input literals, [firstIn] and [secondIn] and an output literal [output].
 */
data class AndGate(val output: Literal, val firstIn: Literal, val secondIn: Literal) {
    override fun toString() = "[$output ← $firstIn ∧ $secondIn]"
}

/**
 * Represents any error that can occur while the [AigerFileParser] is trying to parse an Aiger file. When this exception
 * occurs, it always means that the provided file was malformed in some way. The error message will always contain
 * details about what went wrong.
 */
class AigerParserError(message: String) : Exception(message)
