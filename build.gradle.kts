plugins {
    kotlin("jvm") version "1.4.0"
    application
}

group = "at.tuwien.arrem"
version = "1.0.0"

application {
    mainClassName = "at.tuwien.arrem.demoth.MainKt"
    applicationDefaultJvmArgs = listOf("-Xss2048576")
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib"))

    implementation("org.apache.logging.log4j", "log4j-core", "2.13.3")
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    val main = sourceSets.main.get()

    register<Jar>("buildFatJar") {
        group = "app-backend"
        dependsOn(build)
        manifest {
            attributes["Main-Class"] = "at.tuwien.arrem.demoth.MainKt"
        }

        from(configurations.compileClasspath.get().files.map { if (it.isDirectory) it else zipTree(it) })
        with(jar.get() as CopySpec)
        archiveBaseName.set("${project.name}-fat")
    }
}